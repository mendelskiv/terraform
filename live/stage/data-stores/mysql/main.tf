provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {

      bucket         = "mendelski7-s3"
      key            = "global/s3/terraform.tfstate"
      region         = "us-east-2"
      dynamodb_table = "mendelski7-lock"
      encrypt        = true
  }
}

resource "aws_db_instance" "sedmi" {
  identifier_prefix   = "mendelski7"
  engine              = "mysql"
  allocated_storage   = 10
  instance_class      = "db.t2.micro"
  name                = var.db_name
  username            = "admin"
  password            = var.db_password
  skip_final_snapshot = true
}
